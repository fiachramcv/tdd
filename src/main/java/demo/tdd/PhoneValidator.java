package demo.tdd;

public class PhoneValidator {

	public boolean isNumberValid(String number) {
		if (number == null) {
			throw new IllegalArgumentException("Phone number cannot be null");
		}
		if (number.isEmpty() || number.charAt(0) != '0') {
			return false;
		}
		for (int i = 1; i < number.length(); i++) {
			if (!Character.isDigit(number.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public boolean areNumbersValid(String[] numbers) {
		if (numbers == null) {
			throw new IllegalArgumentException("Array of PhoneNumbers cannot be null");
		}

		for (int i = 0; i < numbers.length; i++) {

			if (numbers[i] == null) {
				throw new IllegalArgumentException("Phone number cannot be null");
			}
			if (numbers[i].isEmpty() || numbers[i].charAt(0) != '0') {
				return false;
			}
			for (int j = 1; j < numbers[i].length(); j++) {
				if (!Character.isDigit(numbers[i].charAt(j))) {
					return false;
				}
			}
		}
		return true;
	}

}
