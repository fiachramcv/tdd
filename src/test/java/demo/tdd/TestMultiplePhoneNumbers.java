package demo.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestMultiplePhoneNumbers {
	
	PhoneValidator val;

	@Before
	public void setUp() throws Exception {
		val = new PhoneValidator();
	}

	@Test
	public void testValidArrayOfPhoneNumbers() {
		String[] good = { "012345", "087654", "077353" };
		assertTrue(val.areNumbersValid(good));
	}
	
	@Test
	public void testInvalidFirstIndexArrayOfPhoneNumbers() {
		String[] bad = { "01 2Abc", "087654", "077353" };
		assertFalse(val.areNumbersValid(bad));
	}
	
	@Test
	public void testInvalidMiddleIndexArrayOfPhoneNumbers() {
		String[] bad = { "012345", "0A8d7 654", "077353" };
		assertFalse(val.areNumbersValid(bad));
	}
	
	@Test
	public void testInvalidLastIndexArrayOfPhoneNumbers() {
		String[] bad = { "012345", "0125454", "07asd7353" };
		assertFalse(val.areNumbersValid(bad));
	}
	
	@Test
	public void testEmptyArrayOfPhoneNumbers() {
		String[] bad = { };
		assertTrue(val.areNumbersValid(bad));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNullArrayOfPhoneNumbers() {
		String[] bad = null;
		val.areNumbersValid(bad);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testArrayOfPhoneNumbersWithNull() {
		String[] bad = { "012345", null, "6546849" };
		val.areNumbersValid(bad);
	}

}
