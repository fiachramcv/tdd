package demo.tdd;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestSinglePhoneNumberMeetsBusinessRules {

	@Test
	public void testCanCreatePhoneValidatorObject() {
		PhoneValidator val = new PhoneValidator();
		assertNotNull(val);
	}
	
	@Test
	public void testThat012345IsAValidPhoneNumber() {
		PhoneValidator val = new PhoneValidator();
		boolean valid = val.isNumberValid("012345");
		assertTrue(valid);
	}

	@Test
	public void testThat12345IsNotAValidPhoneNumber() {
		PhoneValidator val = new PhoneValidator();
		boolean valid = val.isNumberValid("12345");
		assertFalse(valid);
	}
	
	@Test
	public void testThat0ABCIsNotAValidPhoneNumber() {
		PhoneValidator val = new PhoneValidator();
		boolean valid = val.isNumberValid("0ABC");
		assertFalse(valid);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testNullPhoneNumberFails() {
		PhoneValidator val = new PhoneValidator();
		val.isNumberValid(null);
	}

	@Test
	public void testThatPhoneNumberWithSpacesFails() {
		PhoneValidator val = new PhoneValidator();
		boolean valid = val.isNumberValid("0123 213");
		assertFalse(valid);
	}
}
